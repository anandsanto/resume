\documentclass[A4]{Anand-Resume} % Use US Letter paper, change to a4paper for A4 

\begin{document}

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\lastupdated % Print the Last Updated text at the top right

\namesection{Anand}{Narayanan}{ % Your name
\urlstyle{same}\url{www.linkedin.com/in/anandsanto} \\ % Your website, LinkedIn profile or other web address

\href{Mailto}{anandsanto@live.in} | anand.sc13b077@ug.iist.ac.in % Your contact information
\bigskip
\bigskip
\\

}


%----------------------------------------------------------------------------------------
%	LEFT COLUMN
%----------------------------------------------------------------------------------------

\begin{minipage}[t]{0.33\textwidth} % The left column takes up 33% of the text width of the page

%------------------------------------------------
% Education
%------------------------------------------------

\section{Education} 
\bigskip
\subsection{Indian Institute of Space science and Technology}

\descript{B.Tech in Avionics}
\location{Expected May 2017 | Trivandrum,India}
\location{ Cum. GPA: 8.44 / 10.0 \\
Upto Semester 4}
\sectionspace % Some whitespace after the section
\bigskip
%------------------------------------------------

\subsection{S.B.O.A School \& Jr College}
\location{CBSE | 95.6\%}
\location{Grad. May 2012 | Chennai, India}

\sectionspace % Some whitespace after the section

%------------------------------------------------
% Links
%------------------------------------------------

\section{Links} 

Bitbucket:// \href{https://bitbucket.org/anandsanto/}{\bf https://bitbucket.org/anandsanto/} \\
LinkedIn:// \href{www.linkedin.com/in/anandsanto}{\bf www.linkedin.com/in/anandsanto} \\
Quora:// \href{https://www.quora.com/Anand-Narayanan-19}{\bf https://www.quora.com/Anand-Narayanan-19}\\
Facebook:// \href{https://www.facebook.com/anandsanto}{\bf https://www.facebook.com/anandsanto}

\sectionspace % Some whitespace after the section

%------------------------------------------------
% Coursework
%------------------------------------------------

\section{Coursework}

%\subsection{Graduate}

%Advanced Machine Learning \\
%Open Source Software Engineering \\
%Advanced Interactive Graphics \\
%Compilers + Practicum \\
%Cloud Computing

%\sectionspace % Some whitespace after %the section

%------------------------------------------------

%\subsection{Undergraduate}
\bigskip
Computer Organisation \\
Operating Systems \\
Digital Electronics \\
VLSI Design \\
Control Systems and Guidance \\
Digital Signal Processing\\

\sectionspace % Some whitespace after the section

%------------------------------------------------
% Skills
%------------------------------------------------

\section{Skills}
\bigskip
\subsection{Programming}

\location{Over 2000 lines:}
C \textbullet{}  \LaTeX\ \\ 

\location{Over 1000 lines:}
Python \textbullet{} Embedded C \textbullet{} C++ \textbullet{} Verilog \textbullet{} Java \\

\location{Over 500 lines:}
Assembly Language \textbullet{} Matlab\\

\location{Familiar:}
Android

\sectionspace % Some whitespace after the section

%----------------------------------------------------------------------------------------

\end{minipage} % The end of the left column
\hfill
%
%----------------------------------------------------------------------------------------
%	RIGHT COLUMN
%----------------------------------------------------------------------------------------
%
\begin{minipage}[t]{0.63\textwidth} % The right column takes up 66% of the text width of the page

%------------------------------------------------
% Experience
%------------------------------------------------
%------------------------------------------------
% About me
%------------------------------------------------
\section{About me}
\smallskip
As a hard-working person, I am often recognised for my commitment by my
peers and teachers. I love programming and I am fascinated by the art of machine learning
and data analytics. I am absolutely determined to learn whatever it takes to achieve my
goal. I represent my batch in the institute. I usually spend my free time
coding for competitive programming contests, playing chess or sometimes just thinking.
\sectionspace
\section{Projects Undertaken}
\bigskip
\runsubsection{Nano-Satellite Project}
\descript{| On-Board Computing systems }

\location{July 2014 – Present}
\vspace{\topsep} % Hacky fix for awkward extra vertical space
\begin{tightitemize}
\item Using Microsemi Smart Fusion 2 FPGA SoC
\item ARM Cortex M3
\item FreeRTOS Implementation

\end{tightitemize}

\sectionspace % Some whitespace after the section

%------------------------------------------------

\runsubsection{Pothole Detection}
\descript{| Using Microsoft Kinect Sensor}

\location{August 2014 – Present}
\begin{tightitemize}
\item The project aims at automated detection of the potholes in the roads, geo-tagging them and uploading the data to Google Maps 
\item 2 versions of working code has been made, one in C\# and other in Python
\item Machine learning algorithms employed for classification
\item System is near to its completion
\end{tightitemize}

\sectionspace % Some whitespace after the section

%------------------------------------------------
%------------------------------------------------
% Interests
%------------------------------------------------
\section{Topics of Interest}
\bigskip
\begin{tightitemize}
\item Machine Learning 
\item Computer Vision
\item Data Analytics
\item Embedded systems
\item Competitive programming
\item Algorithms
\end{tightitemize}
\sectionspace
%------------------------------------------------
% Skills and Experiences
%------------------------------------------------

\section{Skills and Experiences}
\bigskip
\begin{tightitemize}
\item Hardware programming - Implemented a look-up table based trigonometric
Floating Point Unit in Xilinx FPGA using Verilog.
\item Programming embedded systems - worked with ARM Cortex M3 and AVR
Atmega micro-controllers
\item Hosted a competitive programming contest "C-Cube" as a part of my institute's
national level Tech fest - Conscientia. Also hosted a Treasure Hunt event for my institute's
cultural festival - Dhanak
\item Leadership and management - Class representative of my batch and also an
Executive Board member for the Nano-Satellite project
\item Team Work - working with a 5 member team for the Pothole Detection project
and 30+ member team for the Nano-Satellite project
\end{tightitemize}

\sectionspace % Some whitespace after the section


\sectionspace % Some whitespace after the section

%----------------------------------------------------------------------------------------

\end{minipage} % The end of the right column

%----------------------------------------------------------------------------------------
%	SECOND PAGE (EXAMPLE)
%----------------------------------------------------------------------------------------

%\newpage % Start a new page

%\begin{minipage}[t]{0.33\textwidth} % The left column takes up 33% of the text width of the page

%\section{Example Section}

%\end{minipage} % The end of the left column
%\hfill
%\begin{minipage}[t]{0.66\textwidth} % The right column takes up 66% of the text width of the page

%\section{Example Section 2}

%\end{minipage} % The end of the right column

%----------------------------------------------------------------------------------------

\end{document}